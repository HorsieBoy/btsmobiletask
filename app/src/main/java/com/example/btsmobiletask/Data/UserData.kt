package com.example.btsmobiletask.Data

data class UserData (val uName: String,
                     val uSurname: String,
                     val uLastname: String,
                     val uGender: String,
                     val uPhoto: String,
                     val pass: String,
                     val insurance: String,
                     val bloodType: String)