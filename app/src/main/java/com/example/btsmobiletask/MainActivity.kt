package com.example.btsmobiletask

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Switch
import androidx.annotation.RequiresApi
import java.io.*
import java.lang.Exception
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {

    val UserDataFileName = "userData.txt"
    val emailRegex = "[a-zA-Z0-9].*@[a-zA-Z].*\\.[a-zA-Z].*".toRegex()
    var emailMatch: Boolean = false
    val bloodTypeRegex = "[1-4]{1}[+-]{1}".toRegex()
    var bloodTypeMatch: Boolean = false
    val bloodTypeDict = mapOf("1" to "I", "2" to "II", "3" to "III", "4" to "IV")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val userDataFile = File(UserDataFileName)

        if (userDataFile.exists()) {
            var fileInputStream: FileInputStream? = null
            fileInputStream = openFileInput(UserDataFileName)
            @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS") val inputStreamReader:  InputStreamReader = InputStreamReader(fileInputStream)
            val bufferedReader: BufferedReader = BufferedReader(inputStreamReader)
            val stringBuilder: StringBuilder = StringBuilder()
            var text: String? = null

            while ({text = bufferedReader.readLine()
                    text}() != null){
                stringBuilder.append(text)
            }

            val dataDict = mutableMapOf<String, String>()

            val userData = stringBuilder
                .toString()
                .split("\n").map {
                        line ->
                    line.split("=")
                }.forEach {entry ->
                    dataDict[entry[1]] = entry[2]
                }

            print(dataDict)
            setContentView(R.layout.activity_maps)


        }
        else {
            setContentView(R.layout.settings)

            val gSwitch = findViewById<Switch>(R.id.gender_toggle)
            val gIcon = findViewById<ImageView>(R.id.imageUser)
            gSwitch.setOnCheckedChangeListener {_ , isChecked -> if (!isChecked) {
                gIcon.setImageResource(R.drawable.m)
                println(isChecked)
            } else {
                gIcon.setImageResource(R.drawable.g)
                println(isChecked)
                }
            }

        findViewById<EditText>(R.id.editTextEmail)
            .addTextChangedListener( object: TextWatcher {

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                    Unit
                }

                override fun onTextChanged(
                    s: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    Unit
                }

            override fun afterTextChanged(s: Editable?) {
                if(s != null && s.matches(emailRegex)) {
                    emailMatch = true
                    findViewById<EditText>(R.id.editTextEmail).setBackgroundColor(0)
                } else {
                    println("asdffaasfdf")
                    findViewById<EditText>(R.id.editTextEmail)
                        .setBackgroundColor(Color.parseColor("#ff0000"))
                }
            }
        })

        findViewById<EditText>(R.id.editTextBloodType)
            .addTextChangedListener(object: TextWatcher {

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                    Unit
                }

                override fun onTextChanged(
                    s: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    Unit
                }

                override fun afterTextChanged(s: Editable?) {
                    val bloodTypeView = findViewById<EditText>(R.id.editTextBloodType)
                    if (s != null && s.matches(bloodTypeRegex)) {
                        val bloodType = s.split("")
                        bloodTypeView.setText(bloodTypeDict[bloodType[1]] + "(${bloodType[2]})")
                    } else {
                        bloodTypeView.setText("")
                    }
                }
            })
        }
    }

    fun register(view: View) {
        val nameText = findViewById<EditText>(R.id.editTextName).text.toString()
        val surnameText = findViewById<EditText>(R.id.editTextSurName).text.toString()
        val lastnameText = findViewById<EditText>(R.id.editTextLastName).text.toString()
        val emailText = findViewById<EditText>(R.id.editTextEmail).text.toString()

        val pasportText = findViewById<EditText>(R.id.editTextPassport).text.toString()
        val insuranceText = findViewById<EditText>(R.id.editTextInsurance).text.toString()
        val bloodTypeText = findViewById<EditText>(R.id.editTextBloodType).text.toString()
        val allergyText = findViewById<EditText>(R.id.editTextAllergy).text.toString()

        val dataDict = mapOf("uName" to nameText, "uSurname" to surnameText, "uLastname" to lastnameText, "uEmail" to emailText,
            "uPassport" to pasportText, "uInsurance" to insuranceText, "uBloodType" to bloodTypeText, "uAllergy" to allergyText)

        var dataToWrite = ""

        dataDict.forEach{ (k, v) -> dataToWrite += "$k=$v \n"}

        try {
            val fileOutputStream = openFileOutput(UserDataFileName, Context.MODE_PRIVATE)
            fileOutputStream.write(dataToWrite.toByteArray())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val userDataIntent = Intent(this, MapsActivity::class.java)
        userDataIntent.putExtra("USERDATA", dataToWrite)
        startActivity(userDataIntent)
//        Если есть имя, фамилия и телефон, правильно указан паспорт и полис


    }

}
